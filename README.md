# Tracat 
![](img/dropkit.png)
<br><br>

Ping your cat

## Install

### GPS
```
mkdir tracat && cd tracat
python3 -m venv .env
source .env/bin/activate
pip install adafruit-circuitpython-gps
```
```
cp adafruit_gps.mpy CIRCUITPY/lib
cp adafruit_bus_device CIRCUITPY/lib
```
<br>

Adafruit_gps [API](https://docs.circuitpython.org/projects/gps/en/stable/api.html) <br>
Adafruit_gps [PDF](https://readthedocs.org/projects/adafruit-circuitpython-gps/downloads/pdf/3.5.0/)<br>
[simpletest.py](https://github.com/adafruit/Adafruit_CircuitPython_GPS/blob/main/examples/gps_simpletest.py) <br>
<br>

### Radio
[tutorial](https://learn.adafruit.com/adafruit-feather-m0-radio-with-lora-radio-module/circuitpython-for-rfm9x-lora) <br>
[hookup](*https://learn.adafruit.com/assets/51466) <br>
Headers <br>
[bootloaderinstall](https://learn.adafruit.com/installing-circuitpython-on-samd21-boards) <br>
<br>
Arduino IDE <br>
Package link for SAMD21 https://adafruit.github.io/arduino-board-index/package_adafruit_index.json
<br>
Select Feather M0 <br>
Version of Circuitpython to use [here](https://downloads.circuitpython.org/bin/feather_m0_rfm9x/en_US/adafruit-circuitpython-feather_m0_rfm9x-en_US-8.0.0-beta.6.uf2) <br>

Built-in modules available: adafruit_bus_device, analogio, board, busio, digitalio, math, microcontroller, neopixel_write, nvm, os, pwmio, rainbowio, random, storage, struct, supervisor, time, usb_cdc
Included frozen(?) modules: adafruit_rfm9x <br>
<br> <br>

### Map

```
pip install gpxpy
pip install gpx-cmd-tools

sudo apt-get install gpsbabel-gui
gpsbabel -i unicsv -f input-file.csv -o gpx -F output-file.gpx

pip install gpx_converter
from gpx_converter import Converter
Converter(input_file='your_input.csv').csv_to_gpx(lats_colname='latitude',
```
<br>
import foo.kml in https://www.google.com/maps/d
<br> <br>

### Basic Idea
```

import time
import tracat


catside
tracat.initialize_gps()
tracat.initialize_radio()
while true:
	time.sleep(3)
	tracat.send_location(get_location())

humanside
tracat.initialize_radio()
while true:
	gpxcat(tracat.receive_location()) > foo.cat
	cat foo.gpx | gpx2kml > google_earth_import
```
	
<br> <br>

### Deploy
gitlab ci > bootloader disk
<br> <br>


### Usage
```
% pingcat
% lights [on|off]
% cateye
```

### Power budget
<br>
<br>

RFM69 Transmit cost ma * Duty Cycle ms + Sleep cost  ma
<br>
RP2040 ma
<br>
RPI  ma
<br>
1S Boost > 5V * mAh
2S Buck  > 5V * mAh
Solar	V * mA * 4h

Consider repeater to simplify power requirements, with IP side internal wall power > RFM69 through wall
<br>
<br>



### Radiation Pattern
<br>
<br>
Monopole quarter wave  donut around vertical orthagonal to ground plane
<br>
Dipole
<br>


### Alt Hardware
<br>
https://lowpowerlab.com/shop/product/196
