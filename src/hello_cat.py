import board
import busio
import digitalio
import adafruit_rfm9x
import adafruit_gps


RADIO_FREQ_MHZ = 433.0

CS = digitalio.DigitalInOut(board.D5)
RESET = digitalio.DigitalInOut(board.D6)

# Initialize SPI bus.
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)

# Initialze RFM radio
rfm9x = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)
rfm9x.tx_power = 23

rfm9x.send(bytes("Hello cat\r\n", "utf-8"))
print("Sent Hello cat")
print("Waiting for packets...")

while True:
    packet = rfm9x.receive() # timeout default 0.5 seconds: packet = rfm9x.receive(timeout=5.0)
    if packet is None:
        print("Received nothing! Listening again...")
    else:
        print("Received (raw bytes): {0}".format(packet))
        packet_text = str(packet, "ascii")
        print("Received (ASCII): {0}".format(packet_text))
        rssi = rfm9x.last_rssi
        print("Received signal strength: {0} dB".format(rssi))

