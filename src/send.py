import time
import board
import busio
import digitalio
import adafruit_gps
import adafruit_rfm9x


# Initialize GPS
i2c = board.STEMMA_I2C()
gps = adafruit_gps.GPS_GtopI2C(i2c, debug=False)
gps.send_command(b"PMTK314,0,1,1,1,0,0,0,0,1,0,0,0,0,0,0,0,0,0,0")
time.sleep(1)
gps.send_command(b"PMTK220,1000")

# Initialize radio
RADIO_FREQ_MHZ = 433.0
CS = digitalio.DigitalInOut(board.D5)
RESET = digitalio.DigitalInOut(board.D6)
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
rfm9x = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)
rfm9x.tx_power = 23


def ddm2dd(ddm):
    degrees = int(ddm / 100)
    minutes = ddm % 100
    dd = degrees + minutes / 60
    return dd

# Broadcast location
def send_location():
    while(True):
      time.sleep(1)
      while not(gps.has_fix):
        print(".")
        gps.update()
        time.sleep(1)
      gps.update()
      packet = (str(gps.datetime)) + (str(ddm2dd(gps.latitude_degrees))) + (str(ddm2dd(gps.latitude_minutes))) + ' ' + (str(ToDecimalDegrees(gps.longitude_degrees))) + (str(ToDecimalDegrees(gps.longitude_minutes)))
      print(packet)
      rfm9x.send(packet.encode())

while (True):
    gps.update()
    send_location()
    time.sleep(1)
