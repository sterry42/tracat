import time
import board
import busio
import digitalio
import adafruit_gps
import adafruit_rfm9x


# Initialize radio
RADIO_FREQ_MHZ = 433.0
CS = digitalio.DigitalInOut(board.D5)
RESET = digitalio.DigitalInOut(board.D6)
spi = busio.SPI(board.SCK, MOSI=board.MOSI, MISO=board.MISO)
rfm9x = adafruit_rfm9x.RFM9x(spi, CS, RESET, RADIO_FREQ_MHZ)
rfm9x.tx_power = 23


# Receive octets
def receive_location():
     packet = rfm9x.receive(timeout=2.0) # timeout default 0.5 seconds: packet = rfm9x.receive(timeout=5.0)
     if packet is None:
         print("Nope...")
     else:
         packet_text = str(packet, "ascii")
         print("{0}".format(packet_text))
         #rssi = rfm9x.last_rssi
         #print("{0} dB".format(rssi))

while True:
  receive_location()
  time.sleep(1)
