def ToDecimalDegrees(deg,min,sec):
  decDegrees = float((int(formattedLatLon)) / 100)
  decMinutes = formattedLatLon - (decDegrees * 100)
  fractDegrees = decMinutes / 60.0
  return decDegrees + fractDegrees

See chatgpt code

https://forums.raspberrypi.com/viewtopic.php?t=175163


NMEA outputs coordinates in the ddmm.mmmmm format


https://dateandtime.info/citycoordinates.php?id=5579276
Longmont
40°10'01" N -105°06'06" W
40.1672100° -105.1019300°  decimal degrees
(40.16694444444444, -105.10166666666666)

degrees decimal minutes
Latitude: 40°10.0326′ N
Longitude: 105°6.1158′ W


40° 0.924930', -105° 16.214460'


def convert_gps_to_decimal_coordinates(gps_coordinate: float) -> float:
    dec_coordinate = int(gps_coordinate/100) + (gps_coordinate/100 % 1 ) / 0.6
    return dec_coordinate

gps_coordinate = 40.924930
print("gps_coordinate = ", gps_coordinate)
print("decimal coordinate = ", convert_gps_to_decimal_coordinates(gps_coordinate))







Conversions
import re

def dms2dd(degrees, minutes, seconds, direction):
    dd = float(degrees) + float(minutes)/60 + float(seconds)/(60*60);
    if direction == 'S' or direction == 'W':
        dd *= -1
    return dd;

def dd2dms(deg):
    d = int(deg)
    md = abs(deg - d) * 60
    m = int(md)
    sd = (md - m) * 60
    return [d, m, sd]

def parse_dms(dms):
    parts = re.split('[^\d\w]+', dms)
    lat = dms2dd(parts[0], parts[1], parts[2], parts[3])
    lng = dms2dd(parts[4], parts[5], parts[6], parts[7])

    return (lat, lng)

dd = parse_dms("40°10'01' N 105°06'06' W")

print(dd)
print(dd2dms(dd[0]))
